package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_ILLEGAL_SALE_AMOUNT = "合計金額が10桁を超えました";
	private static final String FILE_BRANCHCODE_NOT_FOUND = "の支店コードが不正です";
	private static final String FILE_COMMODITYCODE_NOT_FOUND = "の商品コードが不正です";
	private static final String RCDFILE_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと合計金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// コマンドライン引数が設定されているか確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}+$", "支店定義")) {
			return;
		}

		// 商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}+$", "商品定義")) {
			return;
		}

		// ファイルの取得と判定
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		// 売上ファイルのファイル名判定に使用
		String namePattern = "^[0-9]{8}.rcd$";

		for(int i = 0; i < files.length; i++) {
			// ファイルかディレクトリなのかを判定し、ファイルなら[rcdFiles]に追加する
			if(files[i].isFile() && files[i].getName().matches(namePattern)) {
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルの連番確認
		Collections.sort(rcdFiles);

		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter =  Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		// 売上ファイルの読み込み
		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {
			File rcdFile = new File(args[0], rcdFiles.get(i).getName());

			String rcdFileName = rcdFiles.get(i).getName();

			try {
				br = new BufferedReader(new FileReader(rcdFile));
				String rcdFileData;

				List<String> rcdFileList = new ArrayList<>();
				while((rcdFileData = br.readLine()) != null) {
					rcdFileList.add(rcdFileData);
				}

				// 売上ファイルのフォーマット確認
				if(rcdFileList.size() != 3) {
					System.out.println(rcdFileName + RCDFILE_INVALID_FORMAT);
					return;
				}

				// 売上ファイルの支店コードと商品コードの存在確認
				String branchCode = rcdFileList.get(0);
				String commodityCode = rcdFileList.get(1);

				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFileName + FILE_BRANCHCODE_NOT_FOUND);
					return;
				} else if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFileName + FILE_COMMODITYCODE_NOT_FOUND);
					return;
				}

				// 売上ファイルの売上金額が数字なのかを判定
				if(!rcdFileList.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long rcdFileSale = Long.parseLong(rcdFileList.get(2));
				Long saleAmount = branchSales.get(branchCode) + rcdFileSale;
				Long commodityAmount = commoditySales.get(commodityCode) + rcdFileSale;

				// 支店・商品それぞれの売上金額の合計桁数の判定
				if((saleAmount >= 10000000000L) || (commodityAmount >= 10000000000L)) {
					System.out.println(FILE_ILLEGAL_SALE_AMOUNT);
					return;
				}

				branchSales.put(branchCode, saleAmount);
				commoditySales.put(commodityCode, commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				if(br != null) {
					try {
						br.close();

					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String matches, String message) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// 支店定義ファイルと商品定義ファイルの存在確認
			if(!file.exists()) {
				System.out.println(message + FILE_NOT_EXIST);
				return false;
			}

			br = new BufferedReader(new FileReader(file));
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				// 支店定義ファイルと商品定義ファイルのフォーマットを確認
				if((items.length != 2) || (!items[0].matches(matches))) {
					System.out.println(message + FILE_INVALID_FORMAT);
					return false;
				}

				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {

				try {
					// ファイルを閉じる
					br.close();

				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		File totalFile = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter(new FileWriter(totalFile));

			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			if(bw != null) {
				try {
					bw.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
